import optparse
import os
import sys
from modules.QuantileRegressor import *
from modules.QuantileCorrector import *


def main():
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-r', '--run', dest='run', help='train/predict [%d]', default='train')
    parser.add_option('-t', dest='trainedInputs', help='trained inputs [%d]', default=None)
    parser.add_option('-i', dest='input', help='input file [%d]',
                      default='/eos/user/c/cdaylewi/attempt2/Chunks/DYJetsToLL_M50_part10.root')
    parser.add_option('-l', dest='label', help='label [%d]', default='recoilqr')
    parser.add_option('-c', dest='configDir', help='configDir [%d]', default="" )
    (opt, args) = parser.parse_args()

    # prepare output
    os.system('mkdir -p %s' % opt.label)

    config = {
    'truthLabels':['SingleMuon_Run2016C'],
    'simLabels':['DYJetsToLL_M50'],
    #'inputDir':'/eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_regress-v2/Chunks/',
    'inputDir':'/eos/user/c/cdaylewi/attempt2/Chunks/',
    'tree':'data',
    'selBranch':'isZ',
    'selVal':1,
    'evtBranches':["nvert","tkmet_sphericity", "tkmet_logpt"],
    'varBranches':["tkmet_pt", 'tkmet_phi'],
    'wgtBranch':"wgt",
    'qList':[0.05,1.0,0.05],
    'displayLoss': False,
    'usePrevVarAsFeature':True,
    'GBRconfig' : {'loss': 'quantile', 'n_estimators': 250, 'max_depth': 20, 'learning_rate': 0.1,
                        'min_samples_leaf': 9, 'min_samples_split': 9}
    }

    if opt.configDir == "":
        try:
            file = open(opt.label+'/config.txt', 'r')
            inputConfig = file.read()
            print(inputConfig)
            file.close()
        except:
            inputConfig = ""
    else:
        inputConfig = opt.configDir

    for key in config: #convert the user config string into the values of the config dictionary.
        if key == 'GBRconfig':
            for subkey in config['GBRconfig']:
                if inputConfig.find(subkey) != -1:
                    temp1 = inputConfig[inputConfig.find(subkey):]
                    start = temp1.find('=')+1
                    end = temp1.find('__')
                    if end == -1:
                        temp2 = temp1[start:]
                    else:
                        temp2 = temp1[start:end]
                    if type(config[key][subkey]) == type(2) or type(config[key][subkey]) == type(0.2):
                        config[key][subkey] = eval(temp2)
                    else:
                        config[key][subkey] = temp2
        else:
            if inputConfig.find(key) != -1:
                temp1 = inputConfig[inputConfig.find(key):]
                start = temp1.find('=')+1
                end = temp1.find('__')
                if end == -1:
                    temp2 = temp1[start:]
                else:
                    temp2 = temp1[start:end]
                if type(config[key]) == type([1,1]) or type(config[key]) == type(2) or type(config[key]) == type(0.2) or type(config[key]) == type(True):
                    config[key] = eval(temp2)
                else:
                    config[key] = temp2


    qr = QuantileRegressor(opt.label, config)

    # train regressor
    if opt.run == 'train':
        qr.loadDF()
        qr.runTrainQuantilesLoop(qList=np.arange(config['qList'][0], config['qList'][1], config['qList'][2]),
                                 usePrevVarAsFeature=config['usePrevVarAsFeature'], GBRconfig=config['GBRconfig'])

    # run prediction
    if opt.run == 'predict':

        # read the regressors
        if not opt.trainedInputs:
            trainedInputs = opt.label+'/train'
        else:
            trainedInputs = opt.trainedInputs
        regList, featureList, mc_w = qr.readQuantileRegressorsWeights(trainedInputs)

        # apply the corrections
        if not opt.input:
            print 'Need to provide input file (-i file)'
            sys.exit(-1)

        # convert the ROOT tree to a pandas DataFrame
        df = rpd.read_root(opt.input, qr.tree, columns=qr.branches)
        print '%d events available in %s' % (len(df.index), opt.input)

        # run corrections
        qc = QuantileCorrector(opt.label, df, regList, featureList, mc_w)
        qc.saveCorrections(os.path.basename(opt.input))


if __name__ == "__main__":
    sys.exit(main())
