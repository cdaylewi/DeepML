#!/bin/bash

TRAINPATH=/afs/cern.ch/work/c/cdaylewi/CMSSW_10_2_0_pre5/src/DeepML

setup_env() {
    cd ${TRAINPATH}
    DEEPJETCORE=${TRAINPATH}/../DeepJetCore
    export PYTHONPATH="${TRAINPATH}/Train:${TRAINPATH}/modules:${DEEPJETCORE}/../:${PYTHONPATH}"

    #setup environment
    if [[ ${TRAINPATH} = *"CMSSW"* ]]; then
        echo "Setting up a CMSSW-based environment"
        eval `scram r -sh`
    else
        echo "Setting up a standalone-based environment"
        export PATH=/afs/cern.ch/user/p/psilva/work/Wmass/train/miniconda/bin:$PATH
        source activate deepjetLinux3
        export LD_PRELOAD=$CONDA_PREFIX/lib/libmkl_core.so:$CONDA_PREFIX/lib/libmkl_sequential.so
    fi

    export LD_LIBRARY_PATH=${DEEPJETCORE}/compiled:$LD_LIBRARY_PATH
    export PATH=${DEEPJETCORE}/bin:$PATH

    ulimit -s 65532
}

print_help() {
    echo "help is here"
}

setup_env

#parse command line
while getopts "h?c:l:" opt; do
    case "$opt" in
    h|\?)
        print_help
        exit 0
        ;;
    c) config=$OPTARG
        ;;
    l) label=$OPTARG
        ;;
    esac
done

echo "${config}"
echo "${label}"


mkdir -p ${label}
touch ${label}/config.txt && echo ${config} > ${label}/config.txt
#python Train/runQuantileRegression.py -l ${label} -r train -c ${config}
python modules/ClassifierTraining.py -l ${label} -c ${config}
#python Train/runQuantileRegression.py -l ${label} -r predict -c ${config}
