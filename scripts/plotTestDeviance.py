import numpy as np
import json
import matplotlib.pyplot as plt
import optparse


"""
This script plots the deviance for the number of trees for the test and train data.
It uses saves the figures to the same location as the trainingDeviance.txt file that is required for this to work.
Perform a training with displayLoss = True for the .txt file to be created.
"""


usage = 'usage: %prog [options]'
parser = optparse.OptionParser(usage)
parser.add_option('-l', dest='label', help='label [%d]', default='recoilct')
(opt, args) = parser.parse_args()

label = opt.label

with open(label+"/trainingDevianvce.txt") as file:
    content = file.readlines()
content = [x.strip() for x in content] #removes whitespaces and \n etc

for i in range(len(content)/3): # loop over the different graphs
    j = i*3
    dictTemp = json.loads(content[j+2]) #convert from json format to dictionary

    plt.plot(dictTemp['n_estimators'],dictTemp['test_set'],"r-",label="test_set")
    plt.plot(dictTemp['n_estimators'],dictTemp['training_set'],"b-",label="training_set")
    plt.title(content[j+1])
    plt.xlabel('n_estimators')
    plt.ylabel('')
    plt.legend(loc = 'upper right')
    plt.savefig(label+"/deviance_"+str(i+1))
    plt.close()
print(str(len(content)/3)+" plots saved to: "+label+"/")

