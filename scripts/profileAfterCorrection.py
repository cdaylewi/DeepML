import ROOT
import math
import optparse
from array import array

usage = 'usage: %prog [options]'
parser = optparse.OptionParser(usage)
parser.add_option('-l', dest='label', help='label [%d]', default='recoilqr1')
parser.add_option('-m', dest='mode', help='mode [%d]', default='save')
(opt, args) = parser.parse_args()

if opt.label == "recoilqr5":
    label = "runrecoilqr5"
    path = "recoilqr5/predict/DYJetsToLL_M50_part10.root"
elif opt.label == "runrecoilqr5":
    label = opt.label
    path = "recoilqr5/predict/DYJetsToLL_M50_part10.root"
else:
    label = opt.label
    path = str(opt.label)+"/predict/DYJetsToLL_M50_part10.root"


if label.find("ct") != -1:
    correctedVar = "tkmet_pt"
    weightVar = "w_"+label
    #weightVar = "kw_"+label
    #weightVar = "w_"+label+"*kw_" + label
else:
    correctedVar = "tkmet_pt_"+label
    weightVar = "w_tkmet_pt"

# file to create plots
# overall distribution #of varBranches and profiles #of

mode = opt.mode
if mode == "save":
    ROOT.gROOT.SetBatch(True)
elif mode == "show":
    ROOT.gROOT.SetBatch(False)
else:
    print("incorrect mode selected, options: '-m save'/'-m show'")

try:
    file = open(label + '/config.txt', 'r')
    inputConfig = file.read()
    file.close()
except:
    inputConfig = ""

if inputConfig.find('evtBranches') != -1:
    temp1 = inputConfig[inputConfig.find('evtBranches'):]
    start = temp1.find('=') + 1
    end = temp1.find('__')
    if end == -1:
        temp2 = temp1[start:]
    else:
        temp2 = temp1[start:end]
    evtBranches = eval(temp2)
else:
    if label.find("ct") != -1:
        evtBranches = ["tkmet_logpt", "ntnpv_logpt", "npvmet_logpt", "tkmet_phi", "tkmet_n", "tkmet_sphericity",
                   "ntnpv_sphericity", "npvmet_sphericity", "absdphi_ntnpv_tk", "dphi_puppi_tk", "rho", "nvert",
                   "mindz", "vz", "nJets"]
    else:
        evtBranches = ["nvert", "tkmet_sphericity"]

if "rho" not in evtBranches:
    evtBranches.append("rho")
if "vis_pt" not in evtBranches:
    evtBranches.append("vis_pt")
if "tkmet_sphericity" not in evtBranches:
    evtBranches.append("tkmet_sphericity")
if "nvert" not in evtBranches:
    evtBranches.append("nvert")
if "vis_eta" not in evtBranches:
    evtBranches.append("vis_eta")


def getProfiles(h,step=1):
    """build the median and 68%CL profiles"""
    try:
        if h.GetEntries()==0 : return None,None
    except:
        return None,None

    medianGr = ROOT.TGraphErrors()
    widthGr  = ROOT.TGraphErrors()

    xq = array('d', [0.16,0.5,0.84])
    yq = array('d', [0.0 ,0.0,0.0 ])

    for xbin in xrange(1,h.GetNbinsX()+1,step):

        tmp=h.ProjectionY('tmp',xbin,xbin+(step-1))
        arr = [tmp.GetBinContent(i) for i in range(tmp.GetNbinsX())]
        testArr = [0.0] * (len(arr))
        if arr == testArr: continue #here we make sure no bins are empty as this results in an integral over 0 warning.
        tmp.GetQuantiles(3,yq,xq)
        xcen=h.GetXaxis().GetBinCenter(xbin)

        npts=medianGr.GetN()
        medianGr.SetPoint(npts,xcen,yq[1])
        medianGr.SetPointError(npts,0,1.2533*tmp.GetMeanError())
        widthGr.SetPoint(npts,xcen,yq[2]-yq[1])
        widthGr.SetPointError(npts,0,tmp.GetRMSError())

    return medianGr,widthGr

#collect data
t1 = ROOT.TChain("tree")
t1.AddFile(path)  # MC - corrected
t2 = ROOT.TChain("data")
t2.AddFile("/eos/user/c/cdaylewi/attempt2/Chunks/DYJetsToLL_M50_part10.root")  # MC -uncorrected
t1.AddFriend(t2)
t3 = ROOT.TChain("data")
t3.AddFile("/eos/user/c/cdaylewi/attempt2/Chunks/SingleMuon_Run2016C_part1.root")  # data

#generate Overall Distribution
maxVal1 = t1.GetMaximum("tkmet_pt")
minVal1 = t1.GetMinimum("tkmet_pt")
maxVal2 = t1.GetMaximum("tkmet_pt")
minVal2 = t1.GetMinimum("tkmet_pt")
maxVal3 = t3.GetMaximum("tkmet_pt")
minVal3 = t3.GetMinimum("tkmet_pt")

maxVal = max([maxVal1, maxVal2, maxVal3])
minVal = min([minVal1, minVal2, minVal3])

c1 = ROOT.TCanvas('c1', 'c1', 500, 500)
h1 = ROOT.TH1F("h1","h1", 100, minVal, maxVal*1.1)
h2 = ROOT.TH1F("h2","h2", 100, minVal, maxVal*1.1)
h3 = ROOT.TH1F("h3","h3", 100, minVal, maxVal*1.1)

t1.Draw(correctedVar+">>h1", "(isZ>0)*"+weightVar, "histnormsame")
t1.Draw("tkmet_pt>>h2", "isZ>0", "histnormsame")
t3.Draw("tkmet_pt>>h3", "isZ>0", "histnormsame")

pValue = h1.Chi2Test(h3, "WW")
chi = h1.Chi2Test(h3, "WWCHI2")
print("P-Value: %5f, Chi^2: %5f"%(pValue,chi))

h1.SetLineColor(2)
h2.SetLineColor(4)
h3.SetLineColor(3)
h1.SetTitle("Overall Distribution")
h1.GetXaxis().SetTitle("Tk-based Recoil (GeV)")

binmax1 = [h1.GetMaximum(), h2.GetMaximum(), h3.GetMaximum()]
binmin1 = [h1.GetMinimum(), h2.GetMinimum(), h3.GetMinimum()]
h1.SetMaximum(max(binmax1)*1.1)
h1.SetMinimum(min(binmin1))

legend1 = ROOT.TLegend(0.6, 0.7, 0.9, 0.9)
legend1.AddEntry(h1, "MC_Corr", "l")
legend1.AddEntry(h2, "MC_Raw", "l")
legend1.AddEntry(h3, "data", "l")
legend1.Draw()

ROOT.gStyle.SetOptStat("0")
c1.Update()
c1.Modify()
if mode == "save":
    c1.SaveAs(opt.label+"/"+opt.label+"_1.png")



#generate Zoomed in distribution
c1z = ROOT.TCanvas('c1z', 'c1z', 500, 500)
h1z = ROOT.TH1F("h1z","h1z", 100, 0, 70)
h2z = ROOT.TH1F("h2z","h2z", 100, 0, 70)
h3z = ROOT.TH1F("h3z","h3z", 100, 0, 70)

t1.Draw(correctedVar+">>h1z", "(isZ>0)*"+weightVar, "histnormsame")
t1.Draw("tkmet_pt>>h2z", "isZ>0", "histnormsame")
t3.Draw("tkmet_pt>>h3z", "isZ>0", "histnormsame")

h1z.SetLineColor(2)
h2z.SetLineColor(4)
h3z.SetLineColor(3)
h1z.SetTitle("Overall Distribution")
h1z.GetXaxis().SetTitle("Tk-based Recoil (GeV)")

binmax1z = [h1z.GetMaximum(), h2z.GetMaximum(), h3z.GetMaximum()]
binmin1z = [h1z.GetMinimum(), h2z.GetMinimum(), h3z.GetMinimum()]
pValue = h1z.Chi2Test(h3z, "WW")
chi = h1z.Chi2Test(h3z, "WWCHI2")
print("P-Value: %5f, Chi^2: %5f"%(pValue,chi))
h1z.SetMaximum(max(binmax1z)*1.1)
h1z.SetMinimum(min(binmin1z))

legend1z = ROOT.TLegend(0.6, 0.7, 0.9, 0.9)
legend1z.AddEntry(h1z, "MC_Corr", "l")
legend1z.AddEntry(h2z, "MC_Raw", "l")
legend1z.AddEntry(h3z, "data", "l")
legend1z.Draw()

c1z.Update()
c1z.Modify()
if mode == "save":
    c1z.SaveAs(opt.label+"/"+opt.label+"_1_zoomed.png")

#Generate profile histograms
canvas = [None]*(len(evtBranches))
canvasW = [None]*(len(evtBranches))
hist = [None]*(len(evtBranches))
gProf = [None]*(len(evtBranches)*3)
g = [None]*(len(evtBranches)*3)
legend = [None]*(len(evtBranches))
legendW = [None]*(len(evtBranches))
medianGr = [None]*(len(evtBranches)*3)
widthGr = [None]*(len(evtBranches)*3)

j=0
k=0

for branches in evtBranches:
    canvas[j] = ROOT.TCanvas('c'+str(j+2), 'c'+str(j+2), 500, 500)
    theVal = min(int(math.ceil(t1.GetMaximum(branches))),int(math.ceil(t3.GetMaximum(branches))))
    hist[j] = ROOT.TH2F("h"+str(j+4), "h"+str(j+4), 20, 0, theVal, 20, 0, 100)  # any

    t1.Draw(correctedVar+":"+branches+">>h"+str(j+4), "(isZ>0)*"+weightVar, "goff")
    medianGr[k], widthGr[k] = getProfiles(hist[j],step=1)
    k+=1

    t1.Draw("tkmet_pt:"+branches+">>h"+str(j+4), "isZ>0", "goff")
    medianGr[k], widthGr[k] = getProfiles(hist[j], step=1)
    k += 1

    t3.Draw("tkmet_pt:"+branches+">>h"+str(j+4), "isZ>0", "goff")
    medianGr[k], widthGr[k] = getProfiles(hist[j], step=1)
    k += 1

    medianGr[k-3].SetLineColor(2)
    medianGr[k - 3].Draw()
    medianGr[k-2].SetLineColor(4)
    medianGr[k - 2].Draw("same")
    medianGr[k - 1].SetLineColor(3)
    medianGr[k - 1].Draw("same")

    binmax2Y = []
    binmax2X = []
    binmax2Y = [ROOT.TMath.MaxElement(medianGr[k-3].GetN(),medianGr[k-3].GetY()), ROOT.TMath.MaxElement(medianGr[k-2].GetN(),medianGr[k-2].GetY()), ROOT.TMath.MaxElement(medianGr[k-1].GetN(),medianGr[k-1].GetY())]
    binmax2X = [ROOT.TMath.MaxElement(medianGr[k-3].GetN(),medianGr[k-3].GetX()), ROOT.TMath.MaxElement(medianGr[k-2].GetN(),medianGr[k-2].GetX()), ROOT.TMath.MaxElement(medianGr[k-1].GetN(),medianGr[k-1].GetX())]
    medianGr[k - 3].GetYaxis().SetRangeUser(0, max(binmax2Y)*1.2)
    medianGr[k - 3].GetXaxis().SetRangeUser(0, max(binmax2X))
    if branches == "tkmet_sphericity":
        medianGr[k - 3].GetXaxis().SetRangeUser(0, 1)
        medianGr[k - 3].GetYaxis().SetRangeUser(0, 40)
    #medianGr[k-3].SetMaximum(max(binmax2)*1.1)
    #medianGr[k-3].SetMinimum(min(binmin2)*0.9)
    medianGr[k-3].SetTitle("Profile "+branches+" median;"+branches+";Median tk-based Recoil (GeV)")

    legend[j] = ROOT.TLegend(0.6, 0.1, 0.9, 0.3)
    legend[j].AddEntry(medianGr[k-3], "MC_Corr", "l")
    legend[j].AddEntry(medianGr[k-2], "MC_Raw", "l")
    legend[j].AddEntry(medianGr[k-1], "data", "l")
    legend[j].Draw()
    # gStyle.SetOptStat("0")
    canvas[j].Update()
    canvas[j].Modify()
    if mode == "save":
        canvas[j].SaveAs(opt.label+"/"+opt.label+"_"+str(j+2)+".png")

    canvasW[j] = ROOT.TCanvas('c'+str(j+2)+'W', 'c'+str(j+2)+'W', 500, 500)
    widthGr[k - 3].SetLineColor(2)
    widthGr[k - 3].Draw()
    widthGr[k - 2].SetLineColor(4)
    widthGr[k - 2].Draw("same")
    widthGr[k - 1].SetLineColor(3)
    widthGr[k - 1].Draw("same")

    binmax2Y = []
    binmax2X = []
    binmax2Y = [ROOT.TMath.MaxElement(widthGr[k-3].GetN(),widthGr[k-3].GetY()), ROOT.TMath.MaxElement(widthGr[k-2].GetN(),widthGr[k-2].GetY()), ROOT.TMath.MaxElement(widthGr[k-1].GetN(),widthGr[k-1].GetY())]
    #binmax2X = [ROOT.TMath.MaxElement(widthGr[k-3].GetN(),widthGr[k-3].GetX()), ROOT.TMath.MaxElement(widthGr[k-2].GetN(),widthGr[k-2].GetX()), ROOT.TMath.MaxElement(widthGr[k-1].GetN(),widthGr[k-1].GetX())]
    binmin2Y = [ROOT.TMath.MinElement(widthGr[k - 3].GetN(), widthGr[k - 3].GetY()),
                ROOT.TMath.MinElement(widthGr[k - 2].GetN(), widthGr[k - 2].GetY()),
                ROOT.TMath.MinElement(widthGr[k - 1].GetN(), widthGr[k - 1].GetY())]
    #binmin2X = [ROOT.TMath.MinElement(widthGr[k - 3].GetN(), widthGr[k - 3].GetX()),
    #            ROOT.TMath.MinElement(widthGr[k - 2].GetN(), widthGr[k - 2].GetX()),
    #            ROOT.TMath.MinElement(widthGr[k - 1].GetN(), widthGr[k - 1].GetX())]
    widthGr[k - 3].GetYaxis().SetRangeUser(min(binmin2Y)*0.8, max(binmax2Y)*1.2)
    #widthGr[k - 3].GetXaxis().SetRangeUser(0, max(binmax2X))
    widthGr[k - 3].SetTitle("Profile " + branches + " width;"+branches+";Width tk-based Recoil (GeV)")

    legendW[j] = ROOT.TLegend(0.6, 0.1, 0.9, 0.3)
    legendW[j].AddEntry(widthGr[k - 3], "MC_Corr", "l")
    legendW[j].AddEntry(widthGr[k - 2], "MC_Raw", "l")
    legendW[j].AddEntry(widthGr[k - 1], "data", "l")
    legendW[j].Draw()
    # gStyle.SetOptStat("0")
    canvasW[j].Update()
    canvasW[j].Modify()
    if mode == "save":
        canvasW[j].SaveAs(opt.label + "/" + opt.label + "_" + str(j + 2) + "W.png")

    j+=1

if mode == "show":
    raw_input()