#!/bin/bash

RED='\033[0;31m'
NC='\033[0m'

TRAINPATH=/afs/cern.ch/user/p/psilva/work/Wmass/train/CMSSW_10_2_0_pre5/src/DeepML

print_help() {
    echo ""
    echo -e "${RED}Usage runRecoilRegression.sh [OPTION]... ${NC}"
    echo "Wraps up environment setting and calls to train or predict models on an input directory with ROOT trees"
    echo ""
    echo "   -r         operation to perform: train, predict"
    echo "   -m         model number to train (see Train/test_TrainData_Recoil.py) or directory with train results for predict"
    echo "   -p         apply model from this directory (required if -r predict)"
    echo "   -o         directory to hold the output [opt]"
    echo "   -c         cut to apply (train only)"
    echo "   -i         directory with input trees or txt file with trees"
    echo "   -t         use this tag to filter input files [opt]"
    echo ""
}

setup_env() {
    cd ${TRAINPATH}
    DEEPJETCORE=${TRAINPATH}/../DeepJetCore
    export PYTHONPATH="${TRAINPATH}/Train:${TRAINPATH}/modules:${DEEPJETCORE}/../:${PYTHONPATH}"
    
    #setup environment
    if [[ ${TRAINPATH} = *"CMSSW"* ]]; then
        echo "Setting up a CMSSW-based environment"
        eval `scram r -sh`
    else    
        echo "Setting up a standalone-based environment"
        export PATH=/afs/cern.ch/user/p/psilva/work/Wmass/train/miniconda/bin:$PATH
        source activate deepjetLinux3
        export LD_PRELOAD=$CONDA_PREFIX/lib/libmkl_core.so:$CONDA_PREFIX/lib/libmkl_sequential.so
    fi

    export LD_LIBRARY_PATH=${DEEPJETCORE}/compiled:$LD_LIBRARY_PATH
    export PATH=${DEEPJETCORE}/bin:$PATH

    ulimit -s 65532
}

run() {

    operation=$1
    rundir=$2/${operation}
    modelTargets=${3}
    cut=$4
    file_list=$5
    modeldir=$6

    mkdir -p ${rundir}

    if [[ "${operation}" == "train" ]]; then
    
        #prepare to define the data
        #variables to use
        varList="tkmet_logpt,ntnpv_logpt,npvmet_logpt,tkmet_phi,tkmet_n"
        varList="${varList},tkmet_sphericity,ntnpv_sphericity,npvmet_sphericity"
        varList="${varList},absdphi_ntnpv_tk,dphi_puppi_tk"
        varList="${varList},rho,nvert,mindz,vz"
        varList="${varList},nJets"
        
        #data class arguments
        modelSpecs=($(echo "${modelTargets}" | tr ":" "\n"))
        classargs="--modelMethod ${modelSpecs[0]} --target ${modelSpecs[1]} --varList ${varList}"
        if [[ "${cut}" != "none" ]]; then
            classargs="${classargs} --sel ${cut}";
        fi

        echo -e "Preparing data to train for model ${RED} ${modelTargets} ${NC} with the following arguments"
        echo -e "\t ${RED} ${classargs} ${NC}"
        trainDataDir=${rundir}/data
        rm -rf ${trainDataDir}
        convertFromRoot.py -i ${file_list} -o ${trainDataDir} -c TrainData_Recoil --noRelativePaths --classArgs "${classargs}"
        
        #now train the model
        echo "Training model"
        trainDir=${rundir}/model
        rm -rf ${trainDir}
        python Train/test_TrainData_Recoil.py ${trainDataDir}/dataCollection.dc ${trainDir} --modelMethod ${modelTargets}
    fi
    
    if [[ -z ${modeldir} ]]; then
        modeldir=${rundir}
    fi

    echo "Testing model"
    testDir=${rundir}/test
    rm -rf ${testDir} 
    convertFromRoot.py --testdatafor ${modeldir}/model/trainsamples.dc -i ${file_list} -o ${testDir} --noRelativePaths

    echo "Running predictions"
    predictDir=${rundir}/predict
    rm -rf ${predictDir}
    predict.py ${modeldir}/model/KERAS_model.h5  ${testDir}/dataCollection.dc ${predictDir}
}

#parse command line
while getopts "h?r:m:o:c:i:t:p:" opt; do
    case "$opt" in
    h|\?)
        print_help
        exit 0
        ;;
    r) operation=$OPTARG
        ;;
    m) model=$OPTARG
        ;;
    p) modeldir=$OPTARG
        ;;
    o) output=$OPTARG
        ;;
    c) cut=$OPTARG
        ;;
    i) input=$OPTARG
        ;;
    t) tag=$OPTARG
        ;;
    esac
done
if [[ -z $operation ]] || [[ -z $model ]] || [[ -z $input ]]; then
    print_help
    exit 0
fi

if [[ "${operation}" == "predict" && -z ${modeldir} ]]; then
    print_help
    exit 0
fi

#start working directory
work=`pwd`/regress_results
mkdir -p ${work}

#append the model string to the output
output=${output}/model${model}
output=${output//[:,]/_}
echo -e "Will run ${RED} ${operation} ${NC} in ${work} and copy to ${output}"

#build list of files to use
if [ -f ${input} ]; then
    file_list=${input}
else
    file_list=${work}/file_list.txt
    rm ${file_list}
    a=(`ls ${input}`)
    for i in ${a[@]}; do
        if [[ -n ${tag} ]]; then
            if [[ $i != *"${tag}"* ]]; then
                continue
            fi
        fi
        echo ${input}/${i} >> ${file_list}
    done
fi
echo "# input files found in ${input} : `cat ${file_list} | wc -l`"

#setup environment
setup_env

#run the required operation
if [[ -z $cut ]]; then
    cut="none";
fi
run ${operation} ${work} ${model} ${cut} ${file_list} ${modeldir}


#prepare output directories
if [ -n "${output}" -a "${output}" != "${work}" ]; then
    mkdir -p ${output}
    echo -e "Moving the results to ${RED} ${output} ${NC}"

    if [[ -d "${work}/${operation}" ]]; then
        cp -rv ${work}/${operation} ${output};

        #update file association to new location
        sed -i.bak s@${work}/${operation}/predict@${output}/${operation}/predict@g ${output}/${operation}/predict/tree_association.txt
    fi
    
    echo -e "Train results have been copied to ${output}"
fi
