import ROOT
import numpy as np
import copy

class ParametricRecoilFunction:

    def __init__(self,model,paramList={},valRange=None,verbose=False):

        """constructor"""

        #configure
        self.model=model
        self.paramList=copy.deepcopy(paramList)
        self.valRange=valRange
        self.verbose=verbose
        
        #compute normalization
        self.norm=self.normalization()


    def normalization(self):
        """computes the normalization"""

        norm=0

        #gaussian double expo
        if self.model=='gd':
            mu,sigma,aL,aR,offset=[self.paramList[x] for x in ['mu','sigma','aL','aR','offset']]
            if self.valRange:                
                tval=[-(ROOT.TMath.Pi()+mu)/sigma,(ROOT.TMath.Pi()-mu)/sigma]
                norm =(ROOT.TMath.Exp(0.5*aL**2)/aL)*(ROOT.TMath.Exp(-aL**2)-ROOT.TMath.Exp(aL*tval[0]))
                norm+=(ROOT.TMath.Exp(0.5*aR**2)/aL)*(ROOT.TMath.Exp(-aR**2)-ROOT.TMath.Exp(-aR*tval[1]))
                norm+=ROOT.TMath.Sqrt(ROOT.TMath.Pi()/2)*(ROOT.TMath.Erf(aL/ROOT.TMath.Sqrt(2))+ROOT.TMath.Erf(aR/ROOT.TMath.Sqrt(2)))
                norm+=(tval[1]-tval[0])*offset
            else:
                norm =1./(aL*ROOT.TMath.Exp(0.5*(aL**2)))
                norm+=1./(aR*ROOT.TMath.Exp(0.5*(aR**2)))
                norm+=ROOT.TMath.Sqrt(ROOT.TMath.Pi()/2)*(ROOT.TMath.Erf(aL/ROOT.TMath.Sqrt(2))+ROOT.TMath.Erf(aR/ROOT.TMath.Sqrt(2)))
            norm *= sigma

        #bifurcated gauss+gauss
        if self.model=='bgsum':
            mu,sigma,sigmaL,fL,sigmaR,fR=[self.paramList[x] for x in ['mu','sigma','sigmaL','fL','sigmaR','fR']]
            if self.valRange:
                norm  = sigma*ROOT.TMath.Erf((ROOT.TMath.Pi()+mu)/(ROOT.TMath.Sqrt(2)*sigma))
                norm += sigma*ROOT.TMath.Erf((ROOT.TMath.Pi()-mu)/(ROOT.TMath.Sqrt(2)*sigma))*(1+fL)/(1+fR)
                norm += fL*sigmaL*ROOT.TMath.Erf((ROOT.TMath.Pi()+mu)/(ROOT.TMath.Sqrt(2)*sigmaL))
                norm += fR*sigmaR*ROOT.TMath.Erf((ROOT.TMath.Pi()-mu)/(ROOT.TMath.Sqrt(2)*sigmaR))*(1+fL)/(1+fR)
            else:
                norm  = sigma*(1+(1+fL)/(1+fR))
                norm += fL*sigmaL
                norm += fR*sigmaR*((1+fL)/(1+fR))
            norm *= ROOT.TMath.Sqrt(ROOT.TMath.Pi()/2)

        return 1./norm

    def eval(self,x):
        """evaluate PDF at a value of x"""

        #check if value with range
        if self.valRange:
            if x>self.valRange[1] or x<self.valRange[0]:
                return 0.

        #gaussian double expo
        if self.model=='gd':
            mu,sigma,aL,aR,offset=[paramList[p] for p in ['mu','sigma','aL','aR','offset']]
            t=(x-mu)/sigma
            ft=offset
            if t<-aL:
                ft+=ROOT.TMath.Exp(aL*(t+0.5*aL))
            elif t>aR:
                ft+=ROOT.TMath.Exp(-aR*(t-0.5*aR))
            else:
                ft+=ROOT.TMath.Exp(-0.5*t*t)

        #bifurcated gauss+gauss
        if self.model=='bgsum':
            mu,sigma,sigmaL,fL,sigmaR,fR=[self.paramList[p] for p in ['mu','sigma','sigmaL','fL','sigmaR','fR']]
            if x<mu:
                ft  = ROOT.TMath.Exp(-0.5*pow((x-mu)/sigma,2))
                ft += fL*ROOT.TMath.Exp(-0.5*pow((x-mu)/sigmaL,2))
            else:
                ft  = ROOT.TMath.Exp(-0.5*pow((x-mu)/sigma,2))
                ft += fR*ROOT.TMath.Exp(-0.5*pow((x-mu)/sigmaR,2))
                ft *= (1+fL)/(1+fR)
        
        #normalize
        ft *= self.norm
        return ft

    def getEventPDF(self,bins):
        
        """returns the event PDF"""

        return [ self.eval(x) for x in bins ]
