# DeepML

Machine learning based on DeepJetCore, etc

## Installation and environment
Follow the installation instructions on https://github.com/DL4Jets/DeepJetCore. Once all is compiled 
then install these scripts to run the regression studies

```
git clone ssh://git@gitlab.cern.ch:7999/psilva/DeepML.git
cd DeepML
source lxplus_env.sh
```

## Running

Use the runRecoilScaleRegression.sh found under scripts. An example below.
Running the script with -h will show all the available options.
```
sh scripts/runRecoilRegression.sh -r train -m 0 -i /eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_regress-v2/Chunks -t WJets -c isW==0
```
If a model exists and you want to extend the predicition to other files than the ones used in the training
you can use the same script but running in predict mode
```
sh scripts/runRecoilRegression.sh -r predict -m 0 -i /eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_regress-v2/Chunks -p regress-results/train
```

## Scripts added and modified by Cavan

To perform classifier training use modules/ClassifierTraining.py

Example includes:
```
python modules/ClassifierTraining.py -l recoilct -c n_estimators=500__displayLoss=True
```
-l corresponds to the label of the particular run you wish to use, I recommend sticking with the format of recoilct## (where ## corresponds to a run number) as this makes plotting down the line easier.

-r corresponds to wether one wants to just 'train', 'predict' or perform 'both'. 'both' is the default.

-c corresponds to the config of the training. The input here must follow a specific format otherwise it will not work. The config is then saved in a .txt file and can be used later for reference.

The default config settings are set as:
- truthLabels=['SingleMuon_Run2016C']
- simLabels=['DYJetsToLL_M50']
- inputDir='/eos/user/c/cdaylewi/attempt2/Chunks/'
- tree='data'
- selBranch='isZ'
- selVal=1
- evtBranches=["tkmet_logpt","ntnpv_logpt","npvmet_logpt","tkmet_phi","tkmet_n","tkmet_sphericity","ntnpv_sphericity","npvmet_sphericity","absdphi_ntnpv_tk","dphi_puppi_tk","rho","nvert","mindz","vz","nJets"],
- wgtBranches='wgt'
- displayLoss=False
- ctFeatures=['vis_pt','vis_eta']
- loss=quantile
- n_estimators=500
- max_depth=10
- learning_rate=0.1
- min\_samples\_leaf=3
- min\_samples\_split=3

If one wants to make a change to say 'n\_estimators' one would use -c n_estimators=500 etc, if one wants to make a change to displayLoss, one would use displayLoss=True.

Say the user wishes to make a change to both 'n\_estimators' and 'displayLoss' one would use -c n\_estimators=500\_\_displayLoss=True. I.e. the double underscore '\_\_' means new variable.

The user has to use the same key names, but may change the values associated with each key. If the input required is a string the user must use single quotation marks e.g. '' as opposed to "".

If the input required is a list one must remember not to use spaces, e.g. -c evtBranches=['nvert','tkmet\_sphericity','tkmet\_logpt'].

A more complicated and valid example is:

-c n\_estimators=500\_\_evtBranches=['nvert','tkmet\_sphericity','tkmet\_logpt']\_\_displayLoss=True

Rules to follow for inputting the config:
- Always use single quotations, not double.
- Never include spaces.
- Separate all items with a double underscore '\_\_'.
- Key names must be exacltly as they are in the default config.
- Use an equal sign to correspond key with value.
- Any unused keys will be left as they are in the default.
- Boolean values must begin with a capital letter.


The script to use to plot the results after prediction is scripts/profileAfterCorrection.py
```
python scripts/profileAfterCorrection.py -l recoilct -m show
```
with -l one must supply the label of the run one wishes to calculate the graphs for. with -m one can say whether they wish to 'show' or 'save' the graphs generated.


Use scripts/plotTestDeviance.py to plot the graphs of the deviance. This only works if displayLoss was set equal to True.
```
python scripts/plotTestDeviance.py -l recoilct
```

Modifications were made to modules/QuantileRegressor.py and modules/QuantileCorrector.py in order to accomadate for an inital classifier correction of the kinematics.

Changes to accomadate this were also made to Train/runQuantileRegression.py, other changes made to runQuantile regression include the ability to use the -c config option to change the config. Althought the config is different from that shown above.
```
python Train/runQuantileRegression.py -l recoilqr -c n\_estimators=500\_\_evtBranches=['nvert','tkmet\_sphericity','tkmet\_logpt']__qList=[0.05,1.0,0.05]
```
--Note: The change of default label name from recoilct (classifier training) to recoilqr (quantile regression).

Submitting to HTCondor:

One will need to edit and use test/condor_submit.sub to submit multiple runs to condor.

One will also need to make changes to scripts/run.sh, as one needs to specify classifer training or quantile regression.


## Diagnostics

Some scripts to do basic plotting are available.
The inputs can be a directory with results or a CSV list of directories with results.
To plot the loss, mse, and all the metrics which were registered use
```
python scripts/makeTrainValidationPlots.py title:regression_results,title:regression_results,...
```
To make ideogram plots (using event-by-event PDF from the regression).
As it usually takes time -n tells the number of events to use (-1=all)
```
python scripts/makeIdeogramPlots.py -i regression_results/train -o ideograms.root  -n -1
```