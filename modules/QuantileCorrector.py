import numpy as np
import os
import ROOT
import array as array
import matplotlib.pyplot as plt

class QuantileCorrector:
   
    """helper class to perfom quantile-based corrections based on the code by P. Musella et al"""

    def __init__(self,label,df,regList,featureList, mc_w):

       #prepare the output directory
       os.system('mkdir -p %s/predict'%label)
       self.label=label

       #store regressors and features
       self.regList=regList
       self.featureList=featureList
       self.mc_w = {}

       print 'Preparing quantile predictions (this may take a while)'
       self.qtls={}
       self.Y={}
       for var in self.regList:

           features=featureList[var]
           print 'Starting with',var
           print 'Features to use are',features

           #project features and run predictions
           self.qtls[var]=[]
           X=df.loc[:,features]
           for i in xrange(0,2):
               print '...running quantile predictions for',('sim' if i==0 else 'truth')
               self.qtls[var].append( np.array( [clf[i+1].predict(X) for clf in regList[var]] ) )

           epsilon = 1.e-3
           clf = mc_w[var]
           print(clf)
           self.mc_w[var] = np.apply_along_axis(lambda x: x[1] / (x[0] + epsilon), 1, clf.predict_proba(X))

           #target variables
           self.Y[var]=df[var]
       
    def saveCorrections(self,outName):

        """loop over the events and correct the target variables"""

        #variables to regress
        varnames=self.Y.keys()

        #open an output file
        fOutName="%s/predict/%s"%(self.label,outName)
        fOut = ROOT.TFile(fOutName, "RECREATE")
        fOut.cd()
        outvarnames=['%s_%s'%(x,self.label) for x in varnames]
        outvarnames += ['w_%s'%x for x in varnames]
        t=ROOT.TNtuple('tree','tree',':'.join(outvarnames))
        print("OUTVARNAMES: ",outvarnames)

        #loop over events and fill tuple with corrections
        nevts=len(self.Y[ varnames[0] ])
        nq=len(self.qtls[varnames[0]][0])
        print 'Looping over %d to save corrected variables'%nevts,varnames
        print nq,'quantiles will be used in the correction'

        # for iev in xrange(0,nevts):
        #     x = {}
        #     xS = {}
        #     yS = {}
        #     xT = {}
        #     yT = {}
        #     for var in self.Y:
        #         simqtls = [self.qtls[var][0][iq][iev] for iq in xrange(0, nq)]
        #         truthqtls = [self.qtls[var][1][iq][iev] for iq in xrange(0, nq)]
        #         # for i in range(len(simqtls)):
        #         #    x.append(i+1)
        #         x[var] = [i for i in range(1, len(simqtls) + 1)]
        #         xS[var], yS[var] = self.calcSpline(simqtls, x[var])  # x and y are swapped
        #         xT[var], yT[var] = self.calcSpline(truthqtls, x[var])
        #
        #         #plt.plot(xS[var],yS[var])
        #         #plt.show()
        #         #raw_input()
        #
        #     ycorr=[self.correctEventSpline(self.Y[var][iev], xS[var],yS[var],xT[var],yT[var]) for var in self.Y]
        #     t.Fill(array.array('f',ycorr))

        for iev in xrange(0,nevts):
           ycorr=[
               self.correctEvent(y=self.Y[var][iev],
                                 simqtls=[self.qtls[var][0][iq][iev] for iq in xrange(0,nq)],
                                 truthqtls=[self.qtls[var][1][iq][iev] for iq in xrange(0,nq)])
               for var in self.Y
               ]
           ycorr += [self.mc_w[var][iev] for var in self.Y]
           t.Fill(array.array('f',ycorr))
        
        #save file
        fOut.cd()
        t.Write()
        fOut.Close()
        print 'Summary tree saved @',fOutName


    def correctEvent(self,y,simqtls,truthqtls):

        """the actual correction algorithm"""

        #find the sim quantile
        qsim =0
        while qsim < len(simqtls): # while + if, to avoid bumping the range
            if simqtls[qsim] < y:
                qsim+=1
            else:
                break

        #determine neighouring quantiles
        if qsim == 0:
            qsim_low,qtruth_low   = 0,0    #lower bound... check this
            qsim_high,qtruth_high = simqtls[qsim],truthqtls[qsim]
        elif qsim < len(simqtls):
            qsim_low,qtruth_low   = simqtls[qsim-1],truthqtls[qsim-1]
            qsim_high,qtruth_high = simqtls[qsim],truthqtls[qsim]
        else:
            qsim_low,qtruth_low   = simqtls[qsim-1],truthqtls[qsim-1]
            qsim_high,qtruth_high = simqtls[len(simqtls)-1]*1.2,truthqtls[len(truthqtls)-1]*1.2
            #sets the value for the highest quantile 20% higher
            
        #linear correction
        return (qtruth_high-qtruth_low)/(qsim_high-qsim_low) * (y - qsim_low) + qtruth_low

    def calcSpline(self,x,y):
        n = len(x)
        A = np.zeros((n, n))
        B = np.zeros((n, 1))
        for i in range(n):
            if i == 0:
                A[i, i] = 2 / float(x[i + 1] - x[i])
                A[i, i + 1] = 1 / float(x[i + 1] - x[i])
                B[i] = 3 * (y[i + 1] - y[i]) / float((x[i + 1] - x[i]) ** 2)
            elif i == n - 1:
                A[i, i - 1] = 1 / float(x[i] - x[i - 1])
                A[i, i] = 2 / float(x[i] - x[i - 1])
                B[i] = 3 * (y[i] - y[i - 1]) / float((x[i] - x[i - 1]) ** 2)
            else:
                A[i, i - 1] = 1 / float(x[i] - x[i - 1])
                A[i, i] = 2 * ((1 / float(x[i] - x[i - 1])) + (1 / float(x[i + 1] - x[i])))
                A[i, i + 1] = 1 / float(x[i + 1] - x[i])
                B[i] = 3 * ((y[i] - y[i - 1]) / float((x[i] - x[i - 1]) ** 2) + (y[i + 1] - y[i]) / float(
                    (x[i + 1] - x[i]) ** 2))

        K = np.dot(np.linalg.inv(A), B)

        def q(t, y1, y2, a, b):
            return (1 - t) * y1 + t * y2 + t * (1 - t) * (a * (1 - t) + b * t)

        xS = np.arange(min(x), max(x), 0.01)
        yS = []
        i = 1
        a = [0.0] * (n - 1)
        b = [0.0] * (n - 1)
        a[i - 1] = K[i - 1][0] * (x[i] - x[i - 1]) - (y[i] - y[i - 1])
        b[i - 1] = -K[i][0] * (x[i] - x[i - 1]) + (y[i] - y[i - 1])
        for xVal in xS:
            t = (xVal - x[i - 1]) / float(x[i] - x[i - 1])
            yS.append(q(t, y[i - 1], y[i], a[i - 1], b[i - 1]))
            if xVal >= x[i]:
                i += 1
                a[i - 1] = K[i - 1][0] * (x[i] - x[i - 1]) - (y[i] - y[i - 1])
                b[i - 1] = -K[i][0] * (x[i] - x[i - 1]) + (y[i] - y[i - 1])
        return xS, yS

    def correctEventSpline(self,x,xS,yS,xT,yT):
        
        #the actual correction algorithm with spline interpolation
        
        #find the sim quantile
        i = 0
        while i < len(xS): # while + if, to avoid bumping the range
            if xS[i] < x:
                i+=1
            else:
                break

        m1 = xS[i]-xS[i-1]
        c1 = yS[i-1]-m1*xS[i-1]
        y = m1*x+c1

        j=0
        while j < len(yT):
            if yT[j] < y:
                j+=1
            else:
                break

        m2 = xT[j]-xT[j-1]
        c2 = yT[j-1]-m2*xT[j-1]
        return (y-c2)/m2