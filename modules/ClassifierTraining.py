import root_pandas as rpd
from root_pandas import read_root
import pickle
import gzip
import numpy as np
from sklearn.ensemble import GradientBoostingRegressor
import pandas as pd
import os
import sys
import time
import re
import optparse
import json
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.utils import shuffle
import ROOT
import array as array

def main():
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-r', '--run', dest='run', help='train/predict/both [%d]', default='both')
    parser.add_option('-t', dest='trainedInputs', help='trained inputs [%d]', default=None)
    #parser.add_option('-i', dest='input', help='input file [%d]', default='/eos/user/c/cdaylewi/attempt2/Chunks/DYJetsToLL_M50_part10.root')
    parser.add_option('-i', dest = 'input', help='input file [%d]', default='/eos/user/c/cdaylewi/attempt2/Chunks/WJetsToLNu_part1.root')
    parser.add_option('-l', dest='label', help='label [%d]', default='recoilct')
    parser.add_option('-c', dest='configDir', help='configDir [%d]', default="" )
    (opt, args) = parser.parse_args()

    # prepare output
    os.system('mkdir -p %s' % opt.label)

    config = {
    'truthLabels':['SingleMuon_Run2016C'],
    'simLabels':['DYJetsToLL_M50'],
    #'inputDir':'/eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_regress-v2/Chunks/',
    'inputDir':'/eos/user/c/cdaylewi/attempt2/Chunks/',
    'tree':'data',
    'selBranch':'isZ',
    'selVal':1,
    #'evtBranches':["nvert","tkmet_sphericity", "tkmet_logpt"],
    #'varBranches':["tkmet_pt", 'tkmet_phi'], # potentially something to do with preg here... not sure
    'evtBranches':["tkmet_logpt","ntnpv_logpt","npvmet_logpt","tkmet_phi","tkmet_n","tkmet_sphericity","ntnpv_sphericity","npvmet_sphericity","absdphi_ntnpv_tk","dphi_puppi_tk","rho","nvert","mindz","vz","nJets"],
    #'evtBranches': ["tkmet_logpt","ntnpv_logpt","npvmet_logpt","tkmet_phi","tkmet_n","tkmet_sphericity","rho","nvert","nJets"],
    'wgtBranch':"wgt",
    'displayLoss':False,
    'ctFeatures':['vis_pt','vis_eta'],
    'GBRconfig' : {'loss': 'quantile', 'n_estimators': 500, 'max_depth': 10, 'learning_rate': 0.1,
                        'min_samples_leaf': 3, 'min_samples_split': 3}
    }

    if opt.configDir == "": #if there is no config entry look for one in a config.txt file #otherwise use default config
        try:
            file = open(opt.label+'/config.txt', 'r')
            inputConfig = file.read()
            print(inputConfig)
            file.close()
        except:
            inputConfig = ""
    else:
        inputConfig = opt.configDir

    for key in config: #convert the config string supplied by user to the config dictionary
        if key == 'GBRconfig':
            for subkey in config['GBRconfig']:
                if inputConfig.find(subkey) != -1:
                    temp1 = inputConfig[inputConfig.find(subkey):]
                    start = temp1.find('=')+1
                    end = temp1.find('__')
                    if end == -1:
                        temp2 = temp1[start:]
                    else:
                        temp2 = temp1[start:end]
                    if type(config[key][subkey]) == type(2) or type(config[key][subkey]) == type(0.2):
                        config[key][subkey] = eval(temp2)
                    else:
                        config[key][subkey] = temp2
        else:
            if inputConfig.find(key) != -1:
                temp1 = inputConfig[inputConfig.find(key):]
                start = temp1.find('=')+1
                end = temp1.find('__')
                if end == -1:
                    temp2 = temp1[start:]
                else:
                    temp2 = temp1[start:end]
                if type(config[key]) == type([1,1]) or type(config[key]) == type(2) or type(config[key]) == type(0.2) or type(config[key]) == type(True):
                    config[key] = eval(temp2)
                else:
                    config[key] = temp2

    ct = ClassifierTraining(opt.label, config) #initialise the classifier training class


    # train regressor
    if opt.run == 'train' or opt.run == 'both':
        ct.loadDF()
        #df = rpd.read_root(opt.input, ct.tree, columns=ct.branches)
        trainedData, kinematicsWeights = ct.runTrainLoop(GBRconfig=config['GBRconfig'])

    # run prediction
    if opt.run == 'predict' or opt.run == 'both':
        if not opt.trainedInputs: #open and read the trained data this in the future may not be neccessary as it may all be stored in memory
            trainedInputs = opt.label+'/train'
        else:
            trainedInputs = opt.trainedInputs
        if opt.run == 'both': #there is the oppertunity to run prediction without having to load the data from the files.
            mc_w = trainedData
            kinematics_w = kinematicsWeights
            featureList = config['evtBranches'][:]
            kinematicFeatureList = config['ctFeatures'][:]
        else:
            print 'Reading from', trainedInputs
            mc_w, kinematics_w, featureList, kinematicFeatureList = ct.readClassifierWeights(trainedInputs)

        # apply the corrections
        if not opt.input:
            print 'Need to provide input file (-i file)'
            sys.exit(-1)

        # convert the ROOT tree to a pandas DataFrame
        df = rpd.read_root(opt.input, ct.tree, columns=ct.branches)
        print '%d events available in %s' % (len(df.index), opt.input)

        # run corrections
        ct.saveCorrections(opt.label, df, featureList,kinematicFeatureList, mc_w, kinematics_w, os.path.basename(opt.input))


class ClassifierTraining:
    def __init__(self,label,config):
        self.label = label
        self.outDir = '%s/train' % self.label
        os.system('mkdir -p %s' % self.outDir)
        print 'QuantileRegressor outputs will be stored in', self.outDir

        # configure the regressor
        print 'Configuration to be used is'
        for key in config:
            setattr(self, key, config[key])
            print key, '=', config[key]

        # branches to extract from the tree
        self.branches = [self.selBranch, self.wgtBranch] + self.evtBranches + self.ctFeatures

        # data to use
        self.truth = None
        self.sim = None

    def loadDF(self, truthF=None, mcF=None):
        """if pickle files are already available use them, otherwise load from ROOT again"""
        if truthF and mcF:
            self.truth = pickle.load(gzip.open(truthF))
            self.sim = pickle.load(gzip.open(mcF))
        else:
            for b in [False, True]:
                self.loadDFFromROOT(isTruth=b)
        ntruth, nsim = len(self.truth.index), len(self.sim.index)
        print 'Events available for truth:%d sim:%d' % (ntruth, nsim)
        if ntruth == 0 or nsim == 0:
            raise ValueError('Invalid number of events')

    def loadDFFromROOT(self, isTruth, savePickle=True):
        """load dataframes from the input files"""

        # Build the list of files
        tagList = self.truthLabels if isTruth else self.simLabels
        fileList = []
        for f in os.listdir(self.inputDir):
            for t in tagList:
                if not t in f: continue
                fileList.append(os.path.join(self.inputDir, f))
        print len(fileList), 'tree files found matching', tagList

        # add trees to a data frame
        for f in fileList:
            try:
                df = pd.concat([df, rpd.read_root(f, self.ttree, columns=self.branches)])
            except:
                df = rpd.read_root(f, self.tree, columns=self.branches)
        print "Events read:", len(df.index)

        # select
        df = df.loc[df[self.selBranch] == self.selVal]
        df = df.drop(columns=[self.selBranch], axis=1)
        df = df.reset_index()
        print "Events selected:", len(df.index)

        # assign selected dataframe
        if isTruth:
            self.truth = df
        else:
            self.sim = df

        # save summary
        if not savePickle: return
        outputName = '%s/qreg_%s.pck' % (self.outDir, 'truth' if isTruth else 'sim')
        pickle.dump(df,
                    gzip.open(outputName, 'wb'),
                    protocol=pickle.HIGHEST_PROTOCOL)
        print 'Pandas dataframe stored in', outputName

    def trainWeights(self, featureList, GBRconfig, weights = []):
        data = self.truth
        mc = self.sim

        clf = GradientBoostingClassifier(n_estimators=GBRconfig['n_estimators'],
                                         # the number of boosting stages to perform
                                         max_depth=GBRconfig['max_depth'],
                                         # maximum depth of the individual regression estimators
                                         learning_rate=GBRconfig['learning_rate'],
                                         # shrinks the contribution of each tree by value here
                                         min_samples_leaf=GBRconfig['min_samples_leaf'],
                                         # minimum number of samples required to be at a leaf node
                                         min_samples_split=GBRconfig['min_samples_split'])

        data_c_train = data[featureList] #select the train data
        mc_c_train = mc[featureList]

        X = np.vstack([data_c_train, mc_c_train]) #vertically stack the data and monte carlo data
        y = np.vstack([np.ones((data_c_train.shape[0], 1)), np.zeros((mc_c_train.shape[0], 1))]) #one corresponds to data and zero corresponds to MC, vertially stack the ones and zeros.

        t0 = time.time()
        if not list(weights): #if no weights supplied i.e. kinematics train
            X, y = shuffle(X, y) #shuffle the vertical stacks of MC and data with their corresponding category
            clf.fit(X,y) #perform the fit
            outputName = '%s/%s_kinematicsWeights.pck' % (self.outDir, self.label)
        else: #if weights are supplied i.e. features training
            weights = np.vstack([np.ones((data_c_train.shape[0], 1)), weights.reshape(weights.shape[0],1)]) #the weights of the data are given as 1
            X, y, weights = shuffle(X, y, weights)
            clf.fit(X, y, weights)
            outputName = '%s/%s_weights.pck' % (self.outDir, self.label)
        t1 = time.time()
        print "Classifier training took = ", t1 - t0, 's'

        if self.displayLoss == True: #if we wish to display the deviance graphs
            t0 = time.time()
            offset = int(X.shape[0] * 0.9)
            #X_train, y_train = X[:offset], y[:offset]
            X_test, y_test = X[offset:], y[offset:]

            #here plot training deviance
            # compute test set deviance
            test_score = np.zeros((GBRconfig['n_estimators'],), dtype=np.float64)

            for i, y_pred in enumerate(clf.staged_predict(X_test)):
              test_score[i] = clf.loss_(y_test, y_pred)

            toPlotDict = {}
            toPlotDict['n_estimators'] = np.arange(GBRconfig['n_estimators']) + 1
            toPlotDict['n_estimators'] = toPlotDict['n_estimators'].tolist()
            toPlotDict['training_set'] = clf.train_score_.tolist() #Training Set Deviance
            toPlotDict['test_set'] = test_score.tolist() #Test Set Deviance
            #save to text file
            file = open(self.label + '/trainingDevianvce.txt', 'a')
            if not list(weights):
                file.write('\nTrain for kinematics\n')
            else:
                file.write('\nTrain for features\n')
            file.write(json.dumps(toPlotDict)+"\n") #converts it to JSON format
            file.close()
            t1 = time.time()
            print 'display loss time = ', t1-t0

        #save training to file
        t0 = time.time()
        with gzip.open(outputName, 'wb') as cache:
            pickle.dump((featureList), cache, protocol=pickle.HIGHEST_PROTOCOL)
            pickle.dump(clf, cache, protocol=pickle.HIGHEST_PROTOCOL)
        print 'Feature weights were saved in', outputName
        t1 = time.time()
        print 'write to file time = ', t1-t0

        return clf

    def runTrainLoop(self, GBRconfig):
        #initially we train the kinematics with out weights, then from this we calculate weights to use in the training of the features

        kinematicsFeatureList = self.ctFeatures[:]
        print 'Starting training of weights for kinematics.'
        clf = self.trainWeights(kinematicsFeatureList, GBRconfig)

        #function to convert training into weights
        featureList = self.evtBranches[:]
        weights = self.predictWeights(self.sim, clf, kinematicsFeatureList)

        print "starting training of weights for full feature list using weights."
        return self.trainWeights(featureList, GBRconfig, weights), clf


    def readClassifierWeights(self, inputDir):
        #Here we read the weights from file
        featureList = {}
        mc_w = {}

        for f in os.listdir(inputDir):
            #if f.find("kinematicsWeights") != -1:
            if "kinematicsWeights" in f:
                fname = os.path.join(inputDir, f)
                with gzip.open(fname, 'rb') as cache:
                    features = pickle.load(cache)
                    mc_w_clf = pickle.load(cache)
                kinematics_w = mc_w_clf
                kinematicFeatureList = features

            if "weights" in f:
                fname = os.path.join(inputDir, f)
                with gzip.open(fname, 'rb') as cache:
                    features = pickle.load(cache)
                    mc_w_clf = pickle.load(cache)
                mc_w = mc_w_clf
                featureList = features

        return mc_w, kinematics_w, featureList, kinematicFeatureList

    def predictWeights(self, df, clf, features):
        #simple function to calculate the weights using clf.predict_proba()
        X=df.loc[:,features]
        epsilon = 1.e-3
        return np.apply_along_axis(lambda x: x[1] / (x[0] + epsilon), 1, clf.predict_proba(X))

    def saveCorrections(self, label, df, featureList, kinematicsFeatureList, mc_w, kinematics_w, outName):
        #Here we save all the corrections to file.
        # prepare the output directory
        os.system('mkdir -p %s/predict' % label)
        self.label = label

        self.mc_w = self.predictWeights(df,mc_w,featureList)
        self.kinematics_w = self.predictWeights(df, kinematics_w, kinematicsFeatureList)

        fOutName = "%s/predict/%s" % (self.label, outName)
        fOut = ROOT.TFile(fOutName, "RECREATE")
        fOut.cd()
        outvarnames = ['w_%s' %  (self.label)]
        outvarnames += ['kw_%s' % (self.label)]
        t = ROOT.TNtuple('tree', 'tree', ':'.join(outvarnames))
        print("OUTVARNAMES: ", outvarnames)

        # loop over events and fill tuple with corrections
        nevts = df[featureList].shape[0]
        print 'Looping over %d to save corrected variables' % nevts

        for iev in xrange(0, nevts):
            ycorr = [self.mc_w[iev]]
            ycorr += [self.kinematics_w[iev]]
            t.Fill(array.array('f', ycorr))

        # save file
        fOut.cd()
        t.Write()
        fOut.Close()
        print 'Summary tree saved @', fOutName

if __name__ == "__main__":
    sys.exit(main())